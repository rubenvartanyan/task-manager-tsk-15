package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullTaskException;
import ru.vartanyan.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task findOneById(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        Task task = taskRepository.findOneByIndex(index);
        if (task == null) throw new NullTaskException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task task = taskRepository.findOneByName(name);
        if (task == null) throw new NullTaskException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task add(final Task task) {
        return taskRepository.add(task);
    }

    @Override
    public Task remove(Task task) {
        return taskRepository.remove(task);
    }

    @Override
    public Task removeOneById(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        Task task = taskRepository.findOneById(id);
        if (task == null) throw new NullTaskException();
        return taskRepository.remove(task);
    }

    @Override
    public Task removeOneByIndex(final Integer index) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        Task task = taskRepository.findOneByIndex(index);
        if (task == null) throw new NullTaskException();
        return taskRepository.remove(task);
    }

    @Override
    public Task removeOneByName(final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task task = taskRepository.findOneByName(name);
        if (task == null) throw new NullTaskException();
        return taskRepository.remove(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task add(String name, String description) throws Exception {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task updateTaskById(final String id,
                               final String name,
                               final String description) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneById(id);
        if (task == null) throw new NullTaskException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final Integer index,
                                  final String name,
                                  final String description) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new NullTaskException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        task.setDateStarted(dateStarted);
        return task;
    }

    @Override
    public Task startTaskByName(String name) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        task.setDateStarted(dateStarted);
        return task;
    }

    @Override
    public Task startTaskByIndex(Integer index) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        final Task task = findOneByIndex(index);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        task.setDateStarted(dateStarted);
        return task;
    }

    @Override
    public Task finishTaskById(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(String name) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(Integer index) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        final Task task = findOneByIndex(index);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task updateTaskStatusById(String id, Status status) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new NullTaskException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task updateTaskStatusByName(String name, Status status) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new NullTaskException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task updateTaskStatusByIndex(Integer index, Status status) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        final Task task = findOneByIndex(index);
        if (task == null) throw new NullTaskException();
        task.setStatus(status);
        return task;
    }

}
