package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.repository.ICommandRepository;
import ru.vartanyan.tm.api.service.ICommandService;
import ru.vartanyan.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
