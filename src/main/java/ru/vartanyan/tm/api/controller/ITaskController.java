package ru.vartanyan.tm.api.controller;

import ru.vartanyan.tm.exception.empty.EmptyIdException;

public interface ITaskController {

    void showList();

    void showTaskByIndex() throws Exception;

    void showTaskByName() throws Exception;

    void showTaskById() throws Exception;

    void removeTaskByIndex() throws Exception;

    void removeTaskById() throws Exception;

    void removeTaskByName() throws Exception;

    void updateTaskByIndex() throws Exception;

    void updateTaskById() throws Exception;

    void create() throws Exception;

    void clear();

    void startTaskById() throws EmptyIdException, Exception;

    void startTaskByName() throws Exception;

    void startTaskByIndex() throws Exception;

    void finishTaskById() throws Exception;

    void finishTaskByName() throws Exception;

    void finishTaskByIndex() throws Exception;

    void updateTaskStatusById() throws Exception;

    void updateTaskStatusByName() throws Exception;

    void updateTaskStatusByIndex() throws Exception;

    void findAllByProjectId() throws Exception;

    void bindTaskByProjectId() throws Exception;

    void unbindTaskByProjectId() throws Exception;

}
