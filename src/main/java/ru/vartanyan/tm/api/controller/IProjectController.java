package ru.vartanyan.tm.api.controller;

public interface IProjectController {

    void showList();

    void showProjectByIndex() throws Exception;

    void showProjectByName() throws Exception;

    void showProjectById() throws Exception;

    void removeProjectByIndex() throws Exception;

    void removeProjectById() throws Exception;

    void removeProjectByName() throws Exception;

    void updateProjectByIndex() throws Exception;

    void updateProjectById() throws Exception;

    void create() throws Exception;

    void clear();

    void startProjectById() throws Exception;

    void startProjectByName() throws Exception;

    void startProjectByIndex() throws Exception;

    void finishProjectById() throws Exception;

    void finishProjectByName() throws Exception;

    void finishProjectByIndex() throws Exception;

    void updateProjectStatusById() throws Exception;

    void updateProjectStatusByName() throws Exception;

    void updateProjectStatusByIndex() throws Exception;

    void removeProjectAndTaskById() throws Exception;

}
