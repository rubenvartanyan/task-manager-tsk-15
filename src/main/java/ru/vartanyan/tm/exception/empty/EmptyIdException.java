package ru.vartanyan.tm.exception.empty;

public class EmptyIdException extends Exception{

    public EmptyIdException() throws Exception {
        super("Error! Id cannot be null or empty...");
    }


}

