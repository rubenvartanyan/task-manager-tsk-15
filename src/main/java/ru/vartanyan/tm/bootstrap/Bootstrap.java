package ru.vartanyan.tm.bootstrap;

import ru.vartanyan.tm.api.controller.ICommandController;
import ru.vartanyan.tm.api.controller.IProjectController;
import ru.vartanyan.tm.api.controller.ITaskController;
import ru.vartanyan.tm.api.repository.ICommandRepository;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.service.*;
import ru.vartanyan.tm.constant.ArgumentConstant;
import ru.vartanyan.tm.constant.TerminalConstant;
import ru.vartanyan.tm.controller.CommandController;
import ru.vartanyan.tm.controller.ProjectController;
import ru.vartanyan.tm.controller.TaskController;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.repository.CommandRepository;
import ru.vartanyan.tm.repository.ProjectRepository;
import ru.vartanyan.tm.repository.TaskRepository;
import ru.vartanyan.tm.service.*;
import ru.vartanyan.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);
    private final ITaskController taskController = new TaskController(taskService, projectTaskService);

    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    void initData() throws Exception {
        projectService.add("BBC", "-").setStatus(Status.NOT_STARTED);
        projectService.add("ABC", "-").setStatus(Status.COMPLETE);
        projectService.add("AAC", "-").setStatus(Status.IN_PROGRESS);
        taskService.add("BBB", "-").setStatus(Status.NOT_STARTED);
        taskService.add("FFF", "-").setStatus(Status.IN_PROGRESS);
        taskService.add("AAA", "-").setStatus(Status.COMPLETE);
    }

    public void run(final String... args) throws Throwable {
        loggerService.debug("TEST!");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initData();
        while (true){
                System.out.println();
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                try {
                    parseCommand(command);
                    System.out.println("[OK]");
                } catch (final Exception e) {
                    loggerService.error(e);
                    System.err.println("[FAIL]");
                }
        }
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConstant.ARG_HELP: commandController.showHelp(); break;
            case ArgumentConstant.ARG_VERSION: commandController.showVersion(); break;
            case ArgumentConstant.ARG_ABOUT: commandController.showAbout(); break;
            case ArgumentConstant.ARG_INFO: commandController.showInfo(); break;
            default: showIncorrectArg();
        }
    }

    public void showIncorrectArg() {
        System.out.println("Error! Argument not found...");
    }

    public void parseCommand(final String arg) throws Throwable {
        if (arg == null) return;
        switch (arg){
            case TerminalConstant.CMD_ABOUT: commandController.showAbout(); break;
            case TerminalConstant.CMD_HELP: commandController.showHelp(); break;
            case TerminalConstant.CMD_VERSION: commandController.showVersion(); break;
            case TerminalConstant.CMD_INFO: commandController.showInfo(); break;
            case TerminalConstant.CMD_EXIT: commandController.exit(); break;
            case TerminalConstant.CMD_COMMANDS: commandController.showCommands(); break;
            case TerminalConstant.CMD_ARGUMENTS: commandController.showArguments(); break;

            case TerminalConstant.CMD_TASK_CREATE: taskController.create(); break;
            case TerminalConstant.CMD_TASK_LIST: taskController.showList(); break;
            case TerminalConstant.CMD_TASK_CLEAR: taskController.clear(); break;

            case TerminalConstant.CMD_PROJECT_CREATE: projectController.create(); break;
            case TerminalConstant.CMD_PROJECT_LIST: projectController.showList(); break;
            case TerminalConstant.CMD_PROJECT_CLEAR: projectController.clear(); break;
            
            case TerminalConstant.CMD_PROJECT_VIEW_BY_ID: projectController.showProjectById(); break;
            case TerminalConstant.CMD_PROJECT_VIEW_BY_NAME: projectController.showProjectByName(); break;
            case TerminalConstant.CMD_PROJECT_VIEW_BY_INDEX: projectController.showProjectByIndex(); break;
            case TerminalConstant.CMD_PROJECT_REMOVE_BY_INDEX: projectController.removeProjectByIndex(); break;
            case TerminalConstant.CMD_PROJECT_REMOVE_BY_ID: projectController.removeProjectById(); break;
            case TerminalConstant.CMD_PROJECT_REMOVE_BY_NAME: projectController.removeProjectByName(); break;
            case TerminalConstant.CMD_PROJECT_UPDATE_BY_ID: projectController.updateProjectById(); break;
            case TerminalConstant.CMD_PROJECT_UPDATE_BY_INDEX: projectController.updateProjectByIndex(); break;

            case TerminalConstant.CMD_TASK_VIEW_BY_ID: taskController.showTaskById(); break;
            case TerminalConstant.CMD_TASK_VIEW_BY_NAME: taskController.showTaskByName(); break;
            case TerminalConstant.CMD_TASK_VIEW_BY_INDEX: taskController.showTaskByIndex(); break;
            case TerminalConstant.CMD_TASK_REMOVE_BY_INDEX: taskController.removeTaskByIndex(); break;
            case TerminalConstant.CMD_TASK_REMOVE_BY_ID: taskController.removeTaskById(); break;
            case TerminalConstant.CMD_TASK_REMOVE_BY_NAME: taskController.removeTaskByName(); break;
            case TerminalConstant.CMD_TASK_UPDATE_BY_ID: taskController.updateTaskById(); break;
            case TerminalConstant.CMD_TASK_UPDATE_BY_INDEX: taskController.updateTaskByIndex(); break;

            case TerminalConstant.CMD_PROJECT_START_BY_ID: projectController.startProjectById(); break;
            case TerminalConstant.CMD_PROJECT_START_BY_NAME: projectController.startProjectByName(); break;
            case TerminalConstant.CMD_PROJECT_START_BY_INDEX: projectController.startProjectByIndex(); break;
            case TerminalConstant.CMD_PROJECT_FINISH_BY_ID: projectController.finishProjectById(); break;
            case TerminalConstant.CMD_PROJECT_FINISH_BY_NAME: projectController.finishProjectByName(); break;
            case TerminalConstant.CMD_PROJECT_FINISH_BY_INDEX: projectController.finishProjectByIndex(); break;
            case TerminalConstant.CMD_PROJECT_UPDATE_STATUS_BY_ID: projectController.updateProjectStatusById(); break;
            case TerminalConstant.CMD_PROJECT_UPDATE_STATUS_BY_NAME: projectController.updateProjectStatusByName(); break;
            case TerminalConstant.CMD_PROJECT_UPDATE_STATUS_BY_INDEX: projectController.updateProjectStatusByIndex(); break;

            case TerminalConstant.CMD_TASK_START_BY_ID: taskController.startTaskById(); break;
            case TerminalConstant.CMD_TASK_START_BY_NAME: taskController.startTaskByName(); break;
            case TerminalConstant.CMD_TASK_START_BY_INDEX: taskController.startTaskByIndex(); break;
            case TerminalConstant.CMD_TASK_FINISH_BY_ID: taskController.finishTaskById(); break;
            case TerminalConstant.CMD_TASK_FINISH_BY_NAME: taskController.finishTaskByName(); break;
            case TerminalConstant.CMD_TASK_FINISH_BY_INDEX: taskController.finishTaskByIndex(); break;
            case TerminalConstant.CMD_TASK_UPDATE_STATUS_BY_ID: taskController.updateTaskStatusById(); break;
            case TerminalConstant.CMD_TASK_UPDATE_STATUS_BY_NAME: taskController.updateTaskStatusByName(); break;
            case TerminalConstant.CMD_TASK_UPDATE_STATUS_BY_INDEX: taskController.updateTaskStatusByIndex(); break;

            case TerminalConstant.CMD_FIND_ALL_TASKS_BY_PROJECT_ID: taskController.findAllByProjectId(); break;
            case TerminalConstant.CMD_BIND_TASK_BY_PROJECT_ID: taskController.bindTaskByProjectId(); break;
            case TerminalConstant.CMD_UNBIND_TASK_BY_PROJECT_ID: taskController.unbindTaskByProjectId(); break;
            case TerminalConstant.CMD_REMOVE_PROJECT_AND_TASKS_BY_PROJECT_ID: projectController.removeProjectAndTaskById(); break;

            default: showIncorrectCommand();
        }
    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }

}
