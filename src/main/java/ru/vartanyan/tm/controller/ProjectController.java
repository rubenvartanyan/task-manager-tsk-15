package ru.vartanyan.tm.controller;
import ru.vartanyan.tm.api.controller.IProjectController;
import ru.vartanyan.tm.api.service.IProjectService;
import ru.vartanyan.tm.api.service.IProjectTaskService;
import ru.vartanyan.tm.enumerated.Sort;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(IProjectService projectService, IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() {
        System.out.println("[PROJECT LIST]");
        System.out.println("[ENTER SORT:]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Project> list;
        if (sort == null || sort.isEmpty()) list = projectService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            list = projectService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Project project: list) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByName() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectById() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        showProject(project);
        System.out.println("[OK]");
    }

    private void showProject(Project project) {
        if (project == null) return;
        System.out.println("[ID] " + project.getId());
        System.out.println("[NAME] " + project.getName());
        System.out.println("[DESCRIPTION] " + project.getDescription());
    }


    @Override
    public void removeProjectByIndex() throws Exception {
        System.out.println("[REMOVE PROJECT");
        System.out.println("[ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeOneByIndex(index);
        System.out.println("[PROJECT REMOVED]");
    }

    @Override
    public void removeProjectById() throws Exception {
        System.out.println("[REMOVE PROJECT");
        System.out.println("[ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(id);
        projectService.removeOneById(id);
        System.out.println("[PROJECT REMOVED]");
    }

    @Override
    public void removeProjectByName() throws Exception {
        System.out.println("[REMOVE PROJECT");
        System.out.println("[ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        System.out.println("[PROJECT REMOVED]");
    }

    @Override
    public void updateProjectByIndex() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[INTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectByIndex(index, name, description);
        System.out.println("[PROJECT UPDATED]");
    }

    @Override
    public void updateProjectById() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER Id]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[INTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectById(id, name, description);
        System.out.println("[PROJECT UPDATED]");
    }

    @Override
    public void create() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER MAME:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = projectService.add(name, description);
        System.out.println("[PROJECT CREATED]");
    }

    @Override
    public void clear() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void startProjectById() throws Exception {
        System.out.println("[START PROJECT]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startProjectById(id);
        System.out.println("[PROJECT STARTED]");
    }

    @Override
    public void startProjectByName() throws Exception {
        System.out.println("[START PROJECT]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startProjectByName(name);
        System.out.println("[PROJECT STARTED]");
    }

    @Override
    public void startProjectByIndex() throws Exception {
        System.out.println("[START PROJECT]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startProjectByIndex(index);
        System.out.println("[PROJECT STARTED]");
    }

    @Override
    public void finishProjectById() throws Exception {
        System.out.println("[FINISH PROJECT]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectById(id);
        System.out.println("[PROJECT FINISHED]");
    }

    @Override
    public void finishProjectByName() throws Exception {
        System.out.println("[START PROJECT]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectByName(name);
        System.out.println("[PROJECT FINISHED]");
    }

    @Override
    public void finishProjectByIndex() throws Exception {
        System.out.println("[FINISH PROJECT]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishProjectByIndex(index);
        System.out.println("[PROJECT FINISHED]");
    }

    @Override
    public void updateProjectStatusById() throws Exception {
        System.out.println("[UPDATE PROJECT STATUS]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.updateProjectStatusById(id, status);
        System.out.println("[PROJECT STATUS UPDATED]");
    }

    @Override
    public void updateProjectStatusByName() throws Exception {
        System.out.println("[UPDATE PROJECT STATUS]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.updateProjectStatusByName(name, status);
        System.out.println("[PROJECT STATUS UPDATED]");
    }

    @Override
    public void updateProjectStatusByIndex() throws Exception {
        System.out.println("[UPDATE PROJECT STATUS]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.updateProjectStatusByIndex(index, status);
        System.out.println("[PROJECT STATUS UPDATED]");
    }

    @Override
    public void removeProjectAndTaskById() throws Exception {
        System.out.println("[REMOVE ALL TASKS FROM PROJECT AND THEN PROJECT]");
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectById(projectId);
        System.out.println("TASKS REMOVED FROM PROJECT");
        System.out.println("PROJECT REMOVED");
    }

}
